#include <iosfwd>
#include <string>
#include <string_view>

namespace dedsec {

enum class KeyEvent {
    left_pressed,
    left_released,
    right_pressed,
    right_released,
    up_pressed,
    up_released,
    down_pressed,
    down_released,
    select_pressed,
    select_released,
    start_pressed,
    start_released,
    buttona_pressed,
    buttona_released,
    buttonb_pressed,
    buttonb_released,
    turn_off
};

std::ostream& operator<<(std::ostream& stream, const KeyEvent e);

class engine;

engine* create_engine();
void destroy_engine(engine* dedeng);

class engine {
public:
    virtual ~engine();
    virtual std::string initialize(std::string_view config) = 0;
    virtual bool read_input(KeyEvent& event) = 0;
    virtual void uninitialize() = 0;
};

}
