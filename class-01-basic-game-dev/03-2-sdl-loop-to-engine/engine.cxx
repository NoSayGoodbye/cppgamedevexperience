#include <algorithm>
#include <array>
#include <exception>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <vector>

#include "engine.hxx"
#include <SDL2/SDL.h>

namespace dedsec {

static std::array<std::string_view, 17> event_titles = {
    { "Button \"LEFT\" was pressed", "Button \"LEFT\" has been released", "Button \"RIGHT\" was pressed", "Button \"RIGHT\" has been released",
        "Button \"UP\" was pressed", "Button \"UP\" has been released", "Button \"DOWN\" was pressed", "Button \"DOWN\" has been released",
        "Button \"SELECT\" was pressed", "Button \"SELECT\" has been released", "Button \"START\" was pressed", "Button \"START\" has been released",
        "Button \"A\" was pressed", "Button \"A\" has been released", "Button \"B\" was pressed", "Button \"B\" has been released", "NES POWER OFF" }
};

std::ostream& operator<<(std::ostream& stream, const KeyEvent event)
{
    std::uint32_t value = static_cast<std::uint32_t>(event);
    std::uint32_t minimal = static_cast<std::uint32_t>(KeyEvent::left_pressed);
    std::uint32_t maximal = static_cast<std::uint32_t>(KeyEvent::turn_off);
    if (value >= minimal && value <= maximal) {
        stream << event_titles[value];
        return stream;
    } else {
        throw std::runtime_error("Too big event value!");
    }
}

#pragma pack(push, 4)
struct BindingKeys {
    SDL_Keycode key;
    std::string_view name;
    KeyEvent event_pressed;
    KeyEvent event_released;
};
#pragma pack(pop)

const std::vector<BindingKeys> keys {
    { { SDLK_UP, "UP", KeyEvent::up_pressed, KeyEvent::up_released },
        { SDLK_LEFT, "LEFT", KeyEvent::left_pressed, KeyEvent::left_released },
        { SDLK_DOWN, "DOWN", KeyEvent::down_pressed, KeyEvent::down_released },
        { SDLK_RIGHT, "RIGHT", KeyEvent::right_pressed, KeyEvent::right_released },
        { SDLK_a, "A", KeyEvent::buttona_pressed,
            KeyEvent::buttona_released },
        { SDLK_b, "B", KeyEvent::buttonb_pressed,
            KeyEvent::buttonb_released },
        { SDLK_SPACE, "SELECT", KeyEvent::select_pressed, KeyEvent::select_released },
        { SDLK_RETURN, "START", KeyEvent::start_pressed, KeyEvent::start_released } }
};

static bool check_input(const SDL_Event& event, const BindingKeys*& result)
{
    const auto iterator = std::find_if(keys.begin(), keys.end(), [&](const BindingKeys& bnd) {
        return bnd.key == event.key.keysym.sym;
    });

    if (iterator != end(keys)) {
        result = &(*iterator);
        return true;
    }
    return false;
}

class engine_impl final : public engine {
public:
    std::string initialize(std::string_view) final
    {
        int window_width = /*1515*/ 320;
        int window_height = /*525*/ 240;
        SDL_Window* const window = SDL_CreateWindow(
            "NES Controller", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, window_width, window_height, 0);

        std::stringstream serr;
        if (window == nullptr) {
            const char* err_mes = SDL_GetError();
            serr << "Error! " << err_mes << std::endl;
            SDL_Quit();
            return serr.str();
        }
        return "";
    }
    bool read_input(KeyEvent& event) final
    {
        SDL_Event sdl_event;
        if (SDL_PollEvent(&sdl_event)) {
            const BindingKeys* binding = nullptr;
            if (sdl_event.type == SDL_QUIT) {
                event = KeyEvent::turn_off;
                return true;
            } else if (sdl_event.type == SDL_KEYDOWN) {
                if (check_input(sdl_event, binding)) {
                    event = binding->event_pressed;
                    return true;
                }
            } else if (sdl_event.type == SDL_KEYUP) {
                if (check_input(sdl_event, binding)) {
                    event = binding->event_released;
                    return true;
                }
            }
        }
        return false;
    }
    void uninitialize() final {}
};

static bool already_exist = false;

engine* create_engine()
{
    if (already_exist) {
        throw std::runtime_error("Dedsec Engine already exist!");
    }
    engine* result = new engine_impl();
    already_exist = true;
    return result;
}

void destroy_engine(engine* dedeng)
{
    if (already_exist == false) {
        throw std::runtime_error("Dedsec Engine not created!");
    }
    if (nullptr == dedeng) {
        throw std::runtime_error("Dedsec Engine is nullptr!");
    }
    delete dedeng;
}

engine::~engine() {}

}
