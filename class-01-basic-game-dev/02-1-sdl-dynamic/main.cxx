#include <iostream>

#include <SDL2/SDL_version.h>

int main(int /*argc*/, char * /*argv*/[]) {
  SDL_version v = {0, 0, 0};

  SDL_GetVersion(&v);

  std::cout << "Installed SDL: " << static_cast<int>(v.major) << "."
            << static_cast<int>(v.minor) << "." << static_cast<int>(v.patch)
            << std::endl;

  bool check = std::cout.good();

  int result = check ? EXIT_SUCCESS : EXIT_FAILURE;
  return result;
}
